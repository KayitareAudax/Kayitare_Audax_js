const mongoose=require("mongoose");
constprovinceschema=new mongoose.Schema({
       provinceId:{
            type:String,
            required:true
        },
       provinceName:{
            type:String,
            required:true
        }
})
module.exports =mongoose.model("provinces_model",provinceschema);