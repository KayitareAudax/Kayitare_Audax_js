const express = require('express');
var app = express();
const config = require('config')
const startupDebug = require('debug')('app:startup');
const dbErrors = require('debug')('app:db');
const bodyparser = require('body-parser');
// const { config } = require('bluebird');
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());
app.get('/', (req, res) => {
    res.send('Welcome to Nation administrative divisions backend Api');
});
// const port = process.env.PORT || 3500;
const apicontrollers=require('../controllers/api_controllers')
app.use('/api', apicontrollers);
// app.listen(port, () => console.log(`Listening on port ${port}..`));
app.listen("app is listening at port: "+config.get("dbConfig.port"));