const express=require('express');
const router=express.Router();
const Joi=require('joi');
const District=require("../models/districts_model")
const Province=require("../models/provinces_model");
const handleDuplicates=function(error, res, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('There was a duplicate key error'));
  } else {
    next();
  }
};
router.post("/province",async(req,res,next)=>{
    const {result,value}=await schema.validate(req.body);
if(result){
    res.send(result.details[0].message);
}
else{
    const newprovince=await new Province({
        provinceId:req.body.provinceId,
        provinceName:req.body.provinceName,
    })

        await newprovince.save()
        .then(added=>{
            res.send(added).status(200)
     .catch(err=>{
            res.status(400).send(err)
        })
})
next();
}
})
router.get("/province",async(req,res,next)=>{
        const provinces=await Province.find()
        .then((result=>{
res.json(result).status(200);
        }))
        .catch((err=>{
console.log(err);
        }))
        
    next()
})
router.get("/province/:id",async(req,res,next) => {
    const findprovince=await Province.find({id:req.params.id})
    .then(findprovince=>{
        res.send(findprovince);
    })
        .catch(err => {
console.log(err);
        })
})
router.delete("/province/:id",async(req,res,next) => {
    const deleted=await Province.remove({_id:req.params.id})
    .then(result => {
        res.send("province deleted").status(203);
        res.send(result);
    })
    .catch(err => {
console.log(err);
    })
    next();
})
router.put("/province",async(req,res,next)=>{
    const {result,value}=await schema.validate(req.body);
if(result){
    res.send(result.details[0].message);
}
else{
        const updated=Province.findOneAndUpdatee({ _id: req.params.id }, req.body, { new: true })
        .then(() =>{
            res.send(updated).status(203)
        })
        .catch(err =>{
            console.log(error);
        })
    }
    next()
})
router.post("/district",async(req,res,next) => {
    const provinceExists=Province.findOne({provinceName:req.body.provinceItIsLocatedIn});
    if(provinceExists){
        const newdistrict=await new District({
            districtId:req.body.districtId,
        districtName:req.body.districtName,
        provinceItIsLocatedIn:req.body.provinceItIsLocatedIn
        })
        await newdistrict.save()
        .then(added => {
            res.send(added);
        })
        .catch(err =>{
            console.log(err);
        })
    }
    else{
        res.status(400).send("the province does not exists");
    }
next()
})
router.get("/district",async(req,res,next)=>{
        const provinces=await District.find()
        .then(result=>{
res.json(districts).status(200);
        })
        .catch(err=>{
            console.log(err);
        })
    next()

})
router.get("/province/:id",async(req,res,next) => {
    const finddistrict=await District.findOne({id:req.params.id})
    .then((district) =>{
        res.send(finddistrict);
    })
        .catch((err) => {
console.log(err);
        })
})
router.delete("/district/:id",async(req,res,next) => {
    const deleted=await District.remove({_id:req.params.id})
    .then((deleted) =>{
        res.send("district deleted").status(203);
        res.send(deleted);
    })
    .catch(err => {
console.log(err);
    })
        
    next();
})
router.put("/district",async(req,res,next)=>{
    const {result,value}=await schema.validate(req.body);
if(result){
    res.send(result.details[0].message);
}
else{
    try {
        const updated=District.findOneAndUpdatee({ _id: req.params.id }, req.body, { new: true });
        res.send(updated).status(203)
    } catch (error) {
        console.log(error);
    }
}
    next()
})
const schema=Joi.object().keys({
  provinceName:Joi.string().min(4).max(45).required(),
  provinceId:Joi.string().min(1).max(145).required(),
  districtId:Joi.string().min(1).max(145).required(),
  districtName:Joi.string().min(4).max(45).required()
})
module.exports=router;