const mongoose=require("mongoose");
const districtschema=new mongoose.Schema({
        districtId:{
            type:String,
            required:true
        },
        districtName:{
            type:String,
            required:true
        },
        provinceItIsLocatedIn:{
            type:String,
            required:true
        }
})
module.exports =mongoose.model("districts_model",districtschema);